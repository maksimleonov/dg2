Чтобы скопировать приложение необходимо сделать следующее.

Скопировать файлы package.json, index.js и папку src
Установить зависимости npm install
Запустить команду npx pod-install ios (для навигации)
Установить шрифты https://github.com/oblador/react-native-vector-icons#installation
Внимательно перечитать про установку react-navigation