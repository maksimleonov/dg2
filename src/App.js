import 'react-native-gesture-handler';
import React from 'react';
import * as firebase from './environment/FirebaseConfig';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';

import store from './store';
import AppContainer from './AppContainer'

export default class App extends React.Component {

    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        );
    }
}


