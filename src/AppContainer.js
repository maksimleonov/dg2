import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import firebase from 'firebase';
import { connect } from 'react-redux';
import FlashMessage from "react-native-flash-message";

import * as actions from './actions';
import styles from './styles';
import MainScreen from './screens/MainScreen';
import BookScreen from './screens/BookScreen';
import MyBookingsScreen from './screens/MyBookingsScreen';
import LoginScreen from './screens/LoginScreen';
import SettingsScreen from './screens/SettingsScreen';
import ConfidentialityScreen from './screens/ConfidentialityScreen';
import DeveloperScreen from './screens/DeveloperScreen';


const stackScreenOptions= {
  cardStyle:{
  ...styles.whiteBackground
  },
  headerTintColor:styles.mainColor.color,
  headerTitleStyle: {
    color:styles.textColor.color,
  },
  headerTruncatedBackTitle:'Назад',
}

const LoginStack = createStackNavigator();

const BookStack = createStackNavigator();
function BookStackScreen() {
    return (
        <BookStack.Navigator
          screenOptions={stackScreenOptions}
        >
            <BookStack.Screen name="Main" component={MainScreen} options={{ title: 'Танцевальная галерея' }} />
            <BookStack.Screen name="Book" component={BookScreen} options={{ title: 'Запись на занятия' }}/>
        </BookStack.Navigator>
    );
}

const MyBookingsStack = createStackNavigator();
function MyBookingsStackScreen() {
    return (
        <MyBookingsStack.Navigator
          screenOptions={stackScreenOptions}
        >
            <MyBookingsStack.Screen name="MyBookingsList" component={MyBookingsScreen} options={{ title: 'Ваши записи' }} />
        </MyBookingsStack.Navigator>
    );
}

const SettingsStack = createStackNavigator();
function SettingsStackScreen() {
    return (
        <SettingsStack.Navigator
          screenOptions={stackScreenOptions}
        >
            <SettingsStack.Screen name="SettingsList" component={SettingsScreen} options={{ title: 'Настройки' }} />
            <SettingsStack.Screen name="Confidentiality" component={ConfidentialityScreen} options={{ title: 'Политика конфиденциальности' }} />
            <SettingsStack.Screen name="Developer" component={DeveloperScreen} options={{ title: 'Разработчик' }} />        
        </SettingsStack.Navigator>
    );
}

const Tab = createBottomTabNavigator();

class AppContainer extends React.Component {

    state={
        user:null
    }

    componentDidMount(){
        // firebase.auth().signOut();
        firebase.auth().onAuthStateChanged((user) => {
          if (user) {
            this.props.signIn(user);
            this.setState({user});
          }
          if (!user) {
            this.setState({user:null});
          }
        });
      }

    render() {
        return (
            <NavigationContainer>
                {!this.state.user ? (
                    <LoginStack.Navigator 
                      screenOptions={stackScreenOptions}
                    >
                    <LoginStack.Screen name="Login" component={LoginScreen} options={{ title: 'Вход в приложение' }}/>
                    </LoginStack.Navigator>
                ) : (
                    <Tab.Navigator 
                    screenOptions={({ route }) => ({
                        tabBarIcon: ({ focused, color, size }) => {
                          let iconName;              
                          if (route.name === 'Book') {
                            iconName = 'ios-calendar'
                          } else if (route.name === 'MyBookings') {
                            iconName = 'ios-list-box'
                          }
                          else if (route.name === 'Settings') {
                            iconName = 'ios-settings'
                          }
                          return <Icon type='ionicon' name={iconName} size={size} color={color} />;
                        },
                      })}
                      tabBarOptions={{
                        activeTintColor: styles.mainColor.color,
                      }}
                    >
                        <Tab.Screen name="Book" component={BookStackScreen} options={{ title: 'Расписание' }}/>
                        <Tab.Screen name="MyBookings" component={MyBookingsStackScreen} options={{ title: 'Ваши записи' }}/>
                        <Tab.Screen name="Settings" component={SettingsStackScreen} options={{ title: 'Настройки' }}/>
                    </Tab.Navigator>
                )}
                <Spinner
                    visible={this.props.loading}
                /> 
                 <FlashMessage position="top" />
            </NavigationContainer>
        );
    }
}

const mapStateToProps = (state) => {
  return{
    loading:state.data.loading
  }
}

export default connect(mapStateToProps, actions)(AppContainer);