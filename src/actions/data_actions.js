export const fetchGroups = (groups) => {
  return (
    {
      type: 'FETCH_GROUPS',
      payload: groups
    }
  );
}

export const fetchUserData = (data) => {
  return (
    {
      type: 'FETCH_USER_DATA',
      payload: data
    }
  );
}

export const startLoading = () => {
  return (
    {
      type: 'START_LOADING',
    }
  );
}

export const stopLoading = () => {
  return (
    {
      type: 'STOP_LOADING',
    }
  );
}