import React from 'react';
import {View, TouchableWithoutFeedback, FlatList, Text } from 'react-native';
import {Button} from 'react-native-elements';
import {CalendarList, LocaleConfig} from 'react-native-calendars';
import moment from 'moment';
import _ from 'lodash';

// const scheduleDates = {
//     '2020-07-16': {disabled: true,},
//     '2020-07-17': {times:['19:00']},
//     '2020-07-18': {disabled: true},
//     '2020-07-19': {times:['19:30', '20:30']}    
// }

//локализация календаря
LocaleConfig.locales['ru'] = {
    monthNames: [
      'Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь',
    ],
    monthNamesShort: [
      'Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек',
    ],
    dayNames: [
      'воскресенье','понедельник','вторник','среда','четверг','пятница','суббота',
    ],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    today: 'Сегодня',
  };
  LocaleConfig.defaultLocale = 'ru';

class BookTime extends React.Component {
    state={
        isTimesShown:false,
        dayTimes:[],
        dayBookingTimes:[],
        selectedDate:null,
        selectedTime:null,
    }

    static defaultProps = {
        buttonTitle:'Записаться',
        styles:{},
        daysShown:60,
        scheduleDates: {},
        onButtonPress : () => {},
    }

    renderTimeItem = (item) => {
        const {styles} = this.props;
        return (
            <Button 
                TouchableComponent = {TouchableWithoutFeedback}
                title={item}
                onPress = {() => this.updateTime(item)}
                buttonStyle = {[
                    styles.multipleButton,
                    this.state.selectedTime === item ? {backgroundColor:styles.mainColor.color} : {backgroundColor:'white'}
                ]}
                titleStyle = {this.state.selectedTime === item ? {color:'white'} : {color:styles.textColor.color}}
                containerStyle={styles.multipleButtonContainer}
                disabled = {this.state.dayBookingTimes.includes(item)}
                icon = {this.state.dayBookingTimes.includes(item) ? {name:'ios-checkmark-circle', type:'ionicon', size:20, color:styles.mainColor.color}: null}
            />
        );
    }

    onDayPress = (day) => {
        const dayTimes = _.get(this.props.scheduleDates, `${day.dateString}.times`) || [];
        const dayBookingTimes = _.get(this.props.scheduleDates, `${day.dateString}.bookingTimes`) || [];
        this.setState({
            dayTimes,
            dayBookingTimes,
            selectedDate:day.dateString,
            selectedTime:null,
         });
    }

    updateTime = (selectedTime) => {
        this.setState({
            selectedTime,
        })
    }

    clearSelection = () => {
        this.setState({
            isTimesShown:false,
            dayTimes:[],
            dayBookingTimes:[],
            selectedDate:null,
            selectedTime:null,
        });
    }

    render(){
        const {styles} = this.props;
        return (
            <React.Fragment>
                <View style={{flex:0.8}}>
                <CalendarList 

                    firstDay={1}
                    minDate={moment().format('YYYY-MM-DD')}
                    maxDate={moment().day(this.props.daysShown).format('YYYY-MM-DD')}
                    // pastScrollRange={0} // глючит отражение в иос из-за этого свойства
                    futureScrollRange={this.props.daysShown/30 }
                    markedDates = {{
                        ...this.props.scheduleDates,
                        [this.state.selectedDate]: {selected: true}
                    }}
                    onDayPress={(day) => this.onDayPress(day)}
                    theme={{
                        backgroundColor:styles.whiteBackground.backgroundColor,
                        calendarBackground: styles.whiteBackground.backgroundColor,
                        selectedDayBackgroundColor:styles.mainColor.color,   
                        todayTextColor:styles.mainColor.color,
                    }}
                />
                </View>
                <View style={[{flex:0.1}, styles.multipleButtonBlockContainer]}>
                    <FlatList
                        data = {this.state.dayTimes}
                        renderItem={({ item }) => this.renderTimeItem(item)}
                        keyExtractor={item => item}
                        horizontal          
                        ListEmptyComponent = {<Text style={styles.mainText}>{
                            this.state.selectedDate ? 'В выбранную дату нет занятий' : 'Выберите дату'
                        }</Text>}              
                    />                 
                </View>
                <View style={[{flex:0.1}, styles.bigButtonBlockContainer]}>
                    <Button 
                        title={this.props.buttonTitle}
                        onPress = {() => {
                            this.props.onButtonPress(this.state.selectedDate, this.state.selectedTime);
                            this.clearSelection();
                        }}
                        disabled = {!this.state.selectedTime}
                        buttonStyle = {styles.mainButton}
                    />
                </View>
            </React.Fragment>
        );
    }
}

export default BookTime;