import React from 'react';
import {View, KeyboardAvoidingView, Image, ScrollView} from 'react-native';
import {Button, Input} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import LoadingOverlay from './LoadingOverlay';
import styles from '../styles';

class EmailAuth extends React.Component {

    static defaultProps = {
        onLoginPress: () => {},
        styles:{},
        ifLogo:false,
        loading:false
    }

    state = {
        email:'',
        password: '',
        signupEmail:'',
        signupPassword:'',
        signupPasswordConfirm:'',
        authMode:'login',
        loadingScreen:false,
        loadingButton:false,
        toastMessage:null
    }

    renderLogin = () => {
        const {styles} = this.props;
        const {email, password} = this.state;
        return (
            <View>
                <View>
                    <Input 
                        label='E-mail'
                        onChangeText={(text) => this.setState({email:text})}
                        value={email}        
                    />
                    <Input 
                        label='Пароль'
                        secureTextEntry
                        onChangeText={(text) => this.setState({password:text})}
                        value={password}  
                    />
                </View>
                <View>
                    <Button
                        title='Войти'
                        onPress = {() => this.props.onLoginPress(email, password)}
                        buttonStyle={styles.mainButton}
                        containerStyle={styles.blockContainer}
                    />
                    <Button
                        title='Нет аккаунта? Зарегистрируйтесь'
                        onPress = {() => this.setState({authMode:'signup'})}
                        type = 'clear'
                        titleStyle={styles.clearButton}
                        containerStyle={styles.blockContainer}
                    />
                </View>
            </View>
        );
    }

    renderSignup = () => {
        const {signupEmail, signupPassword, signupPasswordConfirm} = this.state;
        return (
            <View>
                <View>
                    <Input 
                        label='E-mail'
                        onChangeText={(text) => this.setState({signupEmail:text})}
                        value={signupEmail}        
                    />
                    <Input 
                        label='Пароль'
                        secureTextEntry
                        onChangeText={(text) => this.setState({signupPassword:text})}
                        value={signupPassword}  
                    />
                    <Input 
                        label='Подтвердите пароль'
                        secureTextEntry
                        onChangeText={(text) => this.setState({signupPasswordConfirm:text})}
                        value={signupPasswordConfirm}  
                    />
                </View>
                <View>
                    <Button
                        title='Зарегистрироваться'
                        onPress = {() => this.handleSignupPress()}
                        buttonStyle={styles.mainButton}
                        containerStyle={styles.blockContainer}
                    />
                    <Button
                        title='Уже есть аккаунт? Войдите в приложение'
                        onPress = {() => this.setState({authMode:'login'})}
                        type = 'clear'
                        titleStyle={styles.clearButton}
                        containerStyle={styles.blockContainer}
                    />
                </View>
            </View>        
        );    
    }

    handleSignupPress = () => {
        const {signupEmail, signupPassword, signupPasswordConfirm} = this.state;
        if (signupPassword !== signupPasswordConfirm){
            this.setState({toastMessage:'Пароль не совпадает с подтверждением'});
            return
        }
        this.props.onSignupPress(signupEmail, signupPassword)
    }

    render(){
        const {authMode} = this.state;
        return (
            <KeyboardAvoidingView style={styles.centerAllContainer} behavior="height" enabled>
                <ScrollView
                    contentContainerStyle={styles.centerAllContainer}
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps='handled'
                >
                <View style={styles.inputContainer}>
                <View style={styles.bigMarginContainer}>
                    {this.props.ifLogo &&  <Image
                        style={styles.logo}
                        source={require('../assets/logo.png')}
                        resizeMode = 'contain'
                    />}
                </View>
                {(authMode === 'login') ? this.renderLogin() : this.renderSignup() }
                </View>
                </ScrollView>     
            </KeyboardAvoidingView>
        );
    }
}



export default EmailAuth