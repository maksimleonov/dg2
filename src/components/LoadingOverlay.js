import React from 'react';
import {Overlay} from 'react-native-elements';
import {View, ActivityIndicator, Text} from 'react-native';

const LoadingOverlay = (props) => {
    return (
        <Overlay 
            fullScreen
            isVisible={props.loading}
          >
           <View  style = {{flex:1, justifyContent: 'center', alignItems: 'center'}}>
               <ActivityIndicator size='large' />
            </View>
        </Overlay>
    );
};

LoadingOverlay.defaultProps = {
    loading:false
}

export default LoadingOverlay;