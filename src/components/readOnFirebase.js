import firebase from 'firebase';

// ссылка в аргументе пишется с / в конце
export default async (
    refString,
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}
    ) => {
        startLoadingFunction();
        try {
            firebase.database().ref(refString)
            .on('value', snapshot => {
                stopLoadingFunction();
                successFunction(snapshot.val());
              }
            );
          } catch(error) {
            stopLoadingFunction();
            errorFunction();
          }
};