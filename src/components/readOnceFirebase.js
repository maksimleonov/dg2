import firebase from 'firebase';

// ссылка в аргументе пишется с / в конце
export default async (
    refString,
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}
    ) => {
        startLoadingFunction();
        try {
            let snapshot = await firebase.database().ref(refString).once('value');
            stopLoadingFunction()
            successFunction(snapshot.val());
          } catch(error) {
            stopLoadingFunction();
            errorFunction();
          }
};