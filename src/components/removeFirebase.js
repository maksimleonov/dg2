import firebase from 'firebase';

// ссылка в аргументе пишется с / в конце

export default async (
    refString,
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}
    ) => {
    startLoadingFunction();
    await firebase.database()
    .ref(refString).remove(
        (error) => {
            if (error) {
                stopLoadingFunction();
                errorFunction();
            } else {
                stopLoadingFunction();
                successFunction();
            }
        }
    );
};