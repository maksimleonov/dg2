import firebase from 'firebase';


export default async (
    email,
    password,
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}
    ) => {
        startLoadingFunction();
        try{
            let user = await firebase.auth().signInWithEmailAndPassword(email, password)
            stopLoadingFunction();
            successFunction(user);
        }
        catch(error){
            stopLoadingFunction();
            errorFunction();
        }
};