import firebase from 'firebase';


export default async (
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}
    ) => {
        startLoadingFunction();
        firebase.auth().signOut().then(function() {
            successFunction();
            stopLoadingFunction();
          }).catch(function(error) {
            errorFunction();
            stopLoadingFunction();
          });
};