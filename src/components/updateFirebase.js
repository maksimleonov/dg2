import firebase from 'firebase';

// ссылка в аргументе пишется с / в конце
// для изменения указывается ссылка на конкретный элемент
// для нового элемента ссылка на родительский элемент
export default async (
    updateType,
    baseRefString,
    updatedFields = {},
    errorFunction = ()=>{},
    successFunction = ()=>{},
    startLoadingFunction = ()=>{},
    stopLoadingFunction = ()=>{}

    ) => {
    startLoadingFunction();
    let refString
    if (updateType==='change'){
        refString = baseRefString;
    }
    if (updateType==='create'){
        const baseRef = firebase.database().ref(baseRefString);
        const newKey = baseRef.push().key;
        refString = `${baseRefString}${newKey}`;
    }
    await firebase.database()
    .ref(refString).update(
        updatedFields,
        (error) => {
            if (error) {
                stopLoadingFunction();
                errorFunction();
            } else {
                stopLoadingFunction();
                successFunction();
            }
        }
    );
};