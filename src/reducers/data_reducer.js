const INITIAL_STATE = {
    groups:null,
    userData:null,
    loading:false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case 'FETCH_GROUPS':
        return {
          ...state,
          groups: action.payload
        };
      case 'FETCH_USER_DATA':
        return {
          ...state,
          userData: action.payload
        };
      case 'START_LOADING':
        return {
          ...state,
          loading: true
        };
      case 'STOP_LOADING':
        return {
          ...state,
          loading: false
        };
      default:
        return state;
    }
  };
  