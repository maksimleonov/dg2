import { combineReducers } from 'redux';
import data from './data_reducer';
import auth from './auth_reducer';

export default combineReducers({
  data,
  auth,
});