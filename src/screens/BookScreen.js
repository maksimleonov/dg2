import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import _ from 'lodash';
import moment from 'moment';
import {connect} from 'react-redux';
import { showMessage } from "react-native-flash-message";

import * as actions from '../actions';
import styles from '../styles';
import updateFirebase from '../components/updateFirebase';
import BookTime from '../components/BookTime';

class BookScreen extends React.Component {

    generateDates = () => {
      const {times} = this.props.route.params.group;
      const groupKey = this.props.route.params.group.key;
      let timesByWeekday = {};
      _.map(times, (item, key) =>{
        if (!timesByWeekday[item.weekday]){timesByWeekday[item.weekday]=[]}
        timesByWeekday[item.weekday].push(item.time)
      })
      let scheduleDates = {};
      _.times(60, (n) => {
        const date = moment().day(n).format('YYYY-MM-DD');
        const dateTimes = timesByWeekday[moment(date).isoWeekday()] || [];
        _.assign(scheduleDates, {[date]:{
          times:dateTimes,
          bookingTimes:this.props.bookings[date] || [],
        }});
        return 
      })
      return scheduleDates;

    }

    showSuccessMessage = () => {
      showMessage({
        message: 'Запись выполнена',
        type: "success",
      });
    }

    showErrorMessage = () => {
      showMessage({
        message: 'Ошибка. Запись не выполнена. Попробуйте позже',
        type: "danger",
      });
    }

    handleBookPress = (date, time) => {
      const {key, name} = this.props.route.params.group;
      const {userId} = this.props;
      const booking = {
        groupId:key,
        groupName:name,
        date,
        time
      };
      updateFirebase(
        'create',
        `users/${userId}/bookings/`,
        booking,
        ()=> {this.showErrorMessage()},
        ()=> {this.showSuccessMessage()},
        ()=> {this.props.startLoading()},
        ()=> {this.props.stopLoading()}
      );
    }

    render() {
        return (
          <View style={styles.screenContainer}>
            <BookTime 
              styles={styles}
              onButtonPress = {(date, time) => this.handleBookPress(date, time)}
              scheduleDates = {this.generateDates()}
            />
          </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
  const {user} = state.auth;
  const bookings = state.data.userData ? state.data.userData.bookings : {};
  let bookingsObject = {};
  _.forIn(bookings, (booking, key) =>{
    if (moment(booking.date)>=moment() && ownProps.route.params.group.key === booking.groupId){
      if (!bookingsObject[booking.date]){bookingsObject[booking.date] = []}
      bookingsObject[booking.date].push(booking.time);
    }
  });
  return {
    userId:user.uid,
    bookings:bookingsObject || {},
  };
}

export default connect(mapStateToProps, actions)(BookScreen);