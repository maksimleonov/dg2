import React from 'react';
import {
  View,
  Text,
} from 'react-native';

import styles from '../styles';

class ConfidentialityScreen extends React.Component {

    render(){
        return (
            <View style={styles.blockContainer}>
                <Text style={styles.mainText}>
                    Текст политики конфиденциальности
                </Text>
            </View>
        );
    }
}

export default ConfidentialityScreen