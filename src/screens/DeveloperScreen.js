import React from 'react';
import {
  View,
  Text,
} from 'react-native';

import styles from '../styles';

class DeveloperScreen extends React.Component {

    render(){
        return (
            <View style={styles.blockContainer}>
                <Text style={styles.mainText}>
                    Maksim Leonov, 2020
                </Text>
            </View>
        );
    }
}

export default DeveloperScreen;