import React from 'react';
import {connect} from 'react-redux';
import { showMessage } from "react-native-flash-message";

import * as actions from '../actions';
import EmailAuth from '../components/EmailAuth';
import signInEmailFirebase from '../components/signInEmailFirebase';
import signUpEmailFirebase from '../components/signUpEmailFirebase';
import styles from '../styles';

class LoginScreen extends React.Component {

  showErrorMessage = (text) => {
    showMessage({
      message: text,
      type: "danger",
    });
  }

    render() {
        return <EmailAuth 
          onLoginPress={(email, password) => signInEmailFirebase(
            email, 
            password,
            ()=> {this.showErrorMessage('Не удалось войти')},
            ()=>{},
            ()=> {this.props.startLoading()},
            ()=> {this.props.stopLoading()}
          )}
          onSignupPress={(email, password) => signUpEmailFirebase(
            email, 
            password,
            ()=> {this.showErrorMessage('Не удалось зарегистрироваться')},
            ()=>{},
            ()=> {this.props.startLoading()},
            ()=> {this.props.stopLoading()}
          )}
          styles = {styles}
          ifLogo
        />;
    }
}

export default connect(null, actions)(LoginScreen);