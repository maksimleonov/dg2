import React from 'react';
import {
  View,
  Text,
  FlatList
} from 'react-native';
import { Card, Button, } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import { connect } from 'react-redux';
import _ from 'lodash';
import { showMessage } from "react-native-flash-message";

import * as actions from '../actions';
import readOnceFirebase from '../components/readOnceFirebase';
import readOnFirebase from '../components/readOnFirebase';
import styles from '../styles';

const weekdays = ['Понедельник', "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]


class MainScreen extends React.Component {

    state={
        loading:false
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData = () => {
        readOnceFirebase(
            'groups/',
            ()=> {this.showErrorMessage()},
            (data) => this.props.fetchGroups(data),
            ()=> {this.props.startLoading()},
            ()=> {this.props.stopLoading()}
        );
        readOnFirebase(
            `users/${this.props.userId}/`,
            ()=>{},
            (data) => this.props.fetchUserData(data),
            ()=> {this.props.startLoading()},
            ()=> {this.props.stopLoading()}
        )
    }

    showErrorMessage = () => {
        showMessage({
          message: 'Данные не загружены. Нажмите здесь, чтобы повторить загрузку',
          type: "danger",
          autoHide: false, 

          onPress: () => {
            this.fetchData();
          }
        });
      }

    renderTimes = (times) => {
        const timeItems = _.map(times, (item, key) => {
            return (
                <Text style={styles.cardText} key={key}>
                    {`${weekdays[item.weekday - 1 ]}, ${item.time}`}
                </Text>);
        });
        return timeItems;
    }

    renderGroupCard = (item) => {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.blockContainer}>
            <Card
                containerStyle={styles.cardContainer}
            >
                <Text style={styles.cardHeader}>{item.name}</Text>
                {this.renderTimes(item.times)}
                <Button 
                    buttonStyle={styles.mainButton}
                    title='Записаться'
                    onPress={() => navigate('Book',{
                        group:item
                    })}
                />
            </Card>
        </View>
        );
    }

    render() {
        return (
            <View style={styles.screenContainer}>
                    <FlatList
                        data = {this.props.groupList}
                        renderItem={({ item }) => this.renderGroupCard(item)} 
                    />  
                     <Spinner
                        visible={this.state.loading}
                    /> 
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const groups = state.data.groups;
    const {user} = state.auth;
    const groupList = _.map(groups, (group, key) => {
        const times = _
            .chain(group.schedule)
            .map((time, key) => {return {...time}})
            .sortBy(lesson => [lesson.weekday, lesson.time])
            .value();
        return {
            name:group.name,
            times,
            key
        }
    });
    return {
        groupList,
        userId:user.uid,
    };
}

export default connect(mapStateToProps, actions)(MainScreen);