import React from 'react';
import {
  View,
  Text,
  FlatList
} from 'react-native';
import {ListItem, Button} from 'react-native-elements';
import {connect} from 'react-redux';
import _ from 'lodash';
import { showMessage } from "react-native-flash-message";
import moment from 'moment';
import 'moment/locale/ru'

import * as actions from '../actions';
import removeFirebase from '../components/removeFirebase'
import styles from '../styles';

moment.locale('ru');

class MyBookingsScreen extends React.Component {

  showSuccessMessage = () => {
    showMessage({
      message: 'Запись удалена',
      type: "success",
    });
  }

  showErrorMessage = () => {
    showMessage({
      message: 'Ошибка. Запись не удалена. Попробуйте позже',
      type: "danger",
    });
  }

    removeBooking = (item) => {
      const {userId} = this.props;
      removeFirebase(
        `users/${userId}/bookings/${item.key}`,
        ()=> {this.showErrorMessage()},
        ()=> {this.showSuccessMessage()},
        ()=> {this.props.startLoading()},
        ()=> {this.props.stopLoading()}
      );
    }

    renderBooking = (item) => {
      const {groupName, date, time} = item;
      return <ListItem 
      title={groupName}
      subtitle={`${moment(date).format('L')} ${time}, ${moment(date).format('dddd')}`}
      rightElement={<Button buttonStyle={styles.mainButton} title='Отменить' onPress={()=>{this.removeBooking(item)}}/>}
      bottomDivider
    />
    }

    render() {
        return (
          <View style={[styles.whiteBackground, {flex:1}]}>
            <FlatList
                data = {this.props.bookings}
                renderItem={({ item }) => this.renderBooking(item)} 
                ListEmptyComponent = {<Text style={styles.mainText}>Нет записей на занятия</Text>}              
            />  
          </View>
        );
    }
}

const mapStateToProps = (state) => {
  const {bookings} = state.data.userData || {};
  const {user} = state.auth;
  const sortedBookings = _
  .chain(bookings)
  .map((booking, key) => {return {...booking, key}})
  .sortBy(booking => [booking.date, booking.time])
  .value();
  return {
    bookings:sortedBookings || [],
    userId:user.uid,
  };
}

export default connect(mapStateToProps, actions)(MyBookingsScreen);