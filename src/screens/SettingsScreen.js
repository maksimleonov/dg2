

import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import { connect } from 'react-redux';

import * as actions from '../actions';
import signoutEmailFirebase from '../components/signOutEmailFirebase'

class SettingsScreen extends React.Component {

    handleSignoutPress = () => {
      signoutEmailFirebase(
        ()=>{},
        this.props.signout,
        ()=>{},
        ()=>{},
        ()=> {this.props.startLoading()},
        ()=> {this.props.stopLoading()}
      );
    }

    render() {
        return (
          <View>
            <ListItem
              title='Политика конфиденциальности'
              onPress = {() => {this.props.navigation.navigate('Confidentiality')}}
              bottomDivider
              chevron
            />
            <ListItem
              title='Разработчик'
              onPress = {() => {this.props.navigation.navigate('Developer')}}
              bottomDivider
              chevron
            />
               <ListItem
              title='Выйти из учетной записи'
              onPress = {() => {this.handleSignoutPress()}}
              bottomDivider
              chevron
            />
          </View>
        ); 
    }
}

export default connect(null, actions)(SettingsScreen);