import {StyleSheet} from 'react-native';
import { Dimensions } from 'react-native';

var {height, width} = Dimensions.get('window');
var ratio = width / 100;

const borderRadius=10;
const shadowOffset={
    width:0,
    height:0
}
const shadowOpacity = 0.5;
const shadowRadius = 5;
const mainColor = 'rgb(229, 34, 82)';
const textColor = "#333";
const fontSize = 16;
const padding = 5;
const borderWidth =1;
const borderColor='#ddd'

export default styles=StyleSheet.create({

    blockContainer:{
        padding,       
    },
    centerAllContainer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    bigMarginContainer:{
        margin:50
    },
    inputContainer:{
        paddingLeft:ratio*10,
        paddingRight:ratio*10
    },
    logo:{
        width: ratio*50,
        height: 100,
        alignSelf:'center'
    },
    mainColor:{
        color:mainColor,
    },
    textColor:{
        color:textColor,
    },
    mainText:{
        fontSize,
        color:textColor,
    },
    screenContainer:{
        flex:1,
        padding:5,
    },

    mainButton:{
        backgroundColor:mainColor,
        borderRadius,
    },
    clearButton:{
        color:mainColor
    },
    multipleButton:{
        borderRadius:5,
        borderWidth,
        borderColor
    },
    multipleButtonContainer:{
        padding,
    },
    multipleButtonBlockContainer:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    bigButtonBlockContainer:{
        justifyContent: 'center'
    },
    cardContainer:{
        borderRadius,
        shadowOffset,
        shadowOpacity,
        shadowRadius,
    }, 
    cardHeader:{
        fontWeight:'bold',
        fontSize:20,
        paddingBottom:20
    },
    cardText:{
        color:'#333',
        fontSize:20,
        paddingBottom:10
    },
    whiteBackground:{
        backgroundColor:'white',
    },
    errorText:{
        color:'red'
    }

});